//
//  Constants.swift
//  iOS_car4hire
//
//  Created by Osman Rešidović on 10.03.20.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import Foundation
import UIKit

public struct Constants {
    
    static let supportedLanguages = ["en", "de", "fr", "es", "it"]
    static let APP_LANGUAGES = "AppLanguages"
    
    public struct Endpoints {
        // LOCAL ENV
        static let baseUrl = "http://json-mock.com/endpoint"
        
        static let test = "\(baseUrl)/get/5e67d68c228ec"
        static let errorTest = "\(baseUrl)/get/5e67e5ac7eb60"
    }
    
    public struct Errors {
        static let badResponse = NSError(domain: "NetworkError", code: 500, userInfo: nil)
        static let serializationError = NSError(domain: "NetworkError", code: 501, userInfo: nil)
        static let tokenError = NSError(domain: "LoginError", code: 502, userInfo: ["message": "Login error", "description": "Cannot read token"])
        static let noDataError = NSError(domain: "NetworkError", code: 503, userInfo: ["message": "Network error", "description": "Cannot fetch data from server."])
    }
    
    public struct UserDataKeys {
        static let PICK_UP_LOCATION_KEY = "pick_up_location_key"
        static let CURRENCY_KEY = "currency_key"
        
        static let FIRST_NAME_KEY = "First name"
        static let LAST_NAME_KEY = "Last name"
        static let EMAIL_KEY = "Email"
        static let COUNTRY_KEY = "Country of residence"
        static let MOBILE_KEY = "Mobile phone"
        static let CITY_KEY = "City of residence"
        static let BIRTH_KEY = "Birth date"
        
        static let RESERVATIONS = "reservations"
    }
}
