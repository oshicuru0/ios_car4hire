//
//  ProductResponse.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 21/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class ProductResponse: NSObject {
    var success: Bool?
    var message: String?
    var data: [Product] = []
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        
        for product in dictionary["data"] as! [NSDictionary] {
            data.append(Product(withDictionary: product))
        }
    }
}

class Product: NSObject {
    var id: CLong?
    var currency: String?
    var sponsored: Bool?
    var supplierCurrency: String?
    var rating: Double?
    var depositType: String?
    var depositAmount: Double?
    var car: Car?
    var supplier: Supplier?
    var price: Price?
    var extras: [Extra] = []
    
    init(withDictionary dictionary: NSDictionary) {
        id = dictionary["id"] as? CLong
        currency = dictionary["currency"] as? String
        sponsored = dictionary["sponsored"] as? Bool
        supplierCurrency = dictionary["supplierCurrency"] as? String
        rating = dictionary["rating"] as? Double
        depositType = dictionary["depositType"] as? String
        depositAmount = dictionary["depositAmount"] as? Double
        car = Car(withDictionary: dictionary["car"] as! NSDictionary)
        supplier = Supplier(withDictionary: dictionary["supplier"] as! NSDictionary)
        price = Price(withDictionary: dictionary["price"] as! NSDictionary)
        
        for e in dictionary["extras"] as! [NSDictionary] {
            extras.append(Extra(withDictionary: e))
        }
    }
}

class Car : NSObject {
    var reg: String?
    var acriss: String?
    var manufacturer: String?
    var model: String?
    var doors: String?
    var seats: String?
    var fuelType: String?
    var luggage: String?
    var image: String?
    var transmission: String?
    
    init(withDictionary dictionary: NSDictionary) {
        reg = dictionary["reg"] as? String
        acriss = dictionary["acriss"] as? String
        manufacturer = dictionary["manufacturer"] as? String
        model = dictionary["model"] as? String
        doors = dictionary["doors"] as? String
        seats = dictionary["seats"] as? String
        fuelType = dictionary["fuelType"] as? String
        luggage = dictionary["luggage"] as? String
        image = dictionary["image"] as? String
        transmission = dictionary["transmission"] as? String
    }
}

class Price{
    var daily: Double?
    var days: Int?
    var total: Double?
    var payNow: Double?
    var toSupplier: Double?
    
    init(withDictionary dictionary: NSDictionary) {
        daily = dictionary["daily"] as? Double
        days = dictionary["days"] as? Int
        total = dictionary["total"] as? Double
        payNow = dictionary["payNow"] as? Double
        toSupplier = dictionary["toSupplier"] as? Double
    }
}

class Supplier{
    var id: CLong?
    var name: String?
    var email: String?
    var website: String?
    var phonenumber: String?
    var logo: String?
    var address: String?
    var extras: [SupplierExtra] = []
    
    
    init(withDictionary dictionary: NSDictionary) {
        id = dictionary["id"] as? CLong
        name = dictionary["name"] as? String
        email = dictionary["email"] as? String
        website = dictionary["website"] as? String
        phonenumber = dictionary["phonenumber"] as? String
        logo = dictionary["logo"] as? String
        address = dictionary["address"] as? String
        
        for spe in dictionary["extras"] as! [NSDictionary] {
            extras.append(SupplierExtra(withDictionary: spe))
        }
    }
}

class SupplierExtra{
    var id: CLong?
    var name: String?
    
    init(withDictionary dictionary: NSDictionary) {
        id = dictionary["id"] as? CLong
        name = dictionary["name"] as? String
    }
}

class Extra{
    var id: CLong?
    var name: String?
    var qty: Int?
    var price: Double?
    
    init(withDictionary dictionary: NSDictionary) {
        id = dictionary["id"] as? CLong
        name = dictionary["name"] as? String
        qty = dictionary["qty"] as? Int
        price = dictionary["price"] as? Double
    }
}
