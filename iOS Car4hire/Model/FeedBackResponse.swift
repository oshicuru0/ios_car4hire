//
//  FeedBackResponse.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class FeedBackResponse: NSObject {
    var success: Bool?
    var message: String?
    var data: [Feedback] = []
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        
        for f in dictionary["data"] as? [NSDictionary] ?? [] {
            data.append(Feedback(withDictionary: f))
        }
    }
}

class Feedback: NSObject,Encodable {
    var id: CLong?
    var name: String?
    var date: CLong?
    var review: String?
    var rate: Rating?
    var reservationId: CLong?
    
    init(withDictionary dictionary: NSDictionary) {
        name = dictionary["name"] as? String
        date = dictionary["date"] as? CLong
        review = dictionary["review"] as? String
        rate = Rating(withDictionary: dictionary["rate"] as! NSDictionary)
    }
    
    override init() {
        
    }
}

class Rating: NSObject, Encodable {
    var id: CLong?
    var friendliness: Double?
    var vehicleCleanliness: Double?
    var dropOffTime: Double?
    var overallValue: Double?
    
    init(withDictionary dictionary: NSDictionary) {
        friendliness = dictionary["friendliness"] as? Double
        vehicleCleanliness = dictionary["vehicleCleanliness"] as? Double
        dropOffTime = dictionary["dropOffTime"] as? Double
        overallValue = dictionary["overallValue"] as? Double
    }
    
    override init() {
        
    }
}
