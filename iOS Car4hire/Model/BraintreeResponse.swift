//
//  BraintreeResponse.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 27/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class BraintreeResponse: NSObject {
    var success: Bool?
    var message: String?
    var clientId: String?
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        clientId = (dictionary["data"] as? NSDictionary)?["clientId"] as? String
    }
}
