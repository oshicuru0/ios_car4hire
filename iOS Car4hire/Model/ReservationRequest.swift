//
//  self.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 26/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class ReservationRequest: Codable {
    var id: CLong?
    var productId: CLong?
    var pickUpDate: CLong?
    var dropOffDate: CLong?
    var pickUpLocation: CLong?
    var dropOffLocation: CLong?
    var accepted: Bool = false
    var refused: Bool = false
    var currency: String? = "BAM"
    var lang: String? = "en"
    var email: String?
    var firstName: String?
    var lastName: String?
    var phoneNumber: String?
    var country: String?
    var dateOfBirth: String?
    var cityOfResidence: String?
    var flightNumber: String?
    var childSeat: CLong? = 0
    var gps: CLong? = 0
    var additionalDriver: CLong? = 0
    var nonce: String?
    var transactionId: String?
}
