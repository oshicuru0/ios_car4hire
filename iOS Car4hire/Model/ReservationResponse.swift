//
//  ReservationResponse.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 27/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class ReservationResponse: NSObject {
    var success: Bool!
    var message: String!
    var resrvation: ReservationRequest = ReservationRequest()
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        
        resrvation.id = (dictionary["data"] as! NSDictionary)["id"] as? CLong
        resrvation.productId = (dictionary["data"] as! NSDictionary)["productId"] as? CLong
        resrvation.pickUpDate = (dictionary["data"] as! NSDictionary)["pickUpDate"] as? CLong
        resrvation.dropOffDate = (dictionary["data"] as! NSDictionary)["dropOffDate"] as? CLong
        resrvation.pickUpLocation = (dictionary["data"] as! NSDictionary)["pickUpLocation"] as? CLong
        resrvation.dropOffLocation = (dictionary["data"] as! NSDictionary)["dropOffLocation"] as? CLong
        resrvation.accepted = (dictionary["data"] as! NSDictionary)["accepted"] as? Bool ?? false
        resrvation.refused = (dictionary["data"] as! NSDictionary)["refused"] as? Bool ?? false
        resrvation.currency = (dictionary["data"] as! NSDictionary)["currency"] as? String
        resrvation.email = (dictionary["data"] as! NSDictionary)["email"] as? String
        resrvation.lastName = (dictionary["data"] as! NSDictionary)["lastName"] as? String
        resrvation.firstName = (dictionary["data"] as! NSDictionary)["firstName"] as? String
        resrvation.phoneNumber = (dictionary["data"] as! NSDictionary)["phoneNumber"] as? String
        resrvation.country = (dictionary["data"] as! NSDictionary)["country"] as? String
        resrvation.dateOfBirth = (dictionary["data"] as! NSDictionary)["dateOfBirth"] as? String
        resrvation.cityOfResidence = (dictionary["data"] as! NSDictionary)["cityOfResidence"] as? String
        resrvation.flightNumber = (dictionary["data"] as! NSDictionary)["flightNumber"] as? String
        resrvation.childSeat = (dictionary["data"] as! NSDictionary)["childSeat"] as? CLong
        resrvation.gps = (dictionary["data"] as! NSDictionary)["gps"] as? CLong
        resrvation.additionalDriver = (dictionary["data"] as! NSDictionary)["additionalDriver"] as? CLong
        resrvation.nonce = (dictionary["data"] as! NSDictionary)["nonce"] as? String
        resrvation.transactionId = (dictionary["data"] as! NSDictionary)["transactionId"] as? String
    }
}
