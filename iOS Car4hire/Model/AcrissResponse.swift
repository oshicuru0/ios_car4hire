//
//  AcrissResponse.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class AcrissResponse: NSObject {
    var success: Bool?
    var message: String?
    var data: AcrissData = AcrissData()
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        
        let data = dictionary["data"] as! NSDictionary
        
        var category: [Acriss] = []
        for c in data["category"] as! [NSDictionary] {
            category.append(Acriss(withDictionary: c))
        }
        
        var type: [Acriss] = []
        for c in data["type"] as! [NSDictionary] {
            type.append(Acriss(withDictionary: c))
        }
        self.data.category = category
        self.data.type = type
    }
}

class Acriss: NSObject {
    var name: String?
    var shortName: String?
    
    init(withDictionary dictionary: NSDictionary) {
        name = dictionary["name"] as? String
        shortName = dictionary["shortName"] as? String
    }
}

class AcrissData {
    var category: [Acriss] = []
    var type: [Acriss] = []
}
