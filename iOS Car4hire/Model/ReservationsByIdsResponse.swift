//
//  ReservationsByIdsResponse.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 10/06/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit

class ReservationsByIdsResponse: NSObject {
    var success: Bool?
    var message: String?
    var data: [ReservationWithPdf] = [ReservationWithPdf]()
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        
        for d in dictionary["data"] as! [NSDictionary] {
            data.append(ReservationWithPdf(withDictionary: d))
        }
    }
}

class ReservationWithPdf {
    var pdf: String?
    var id: CLong?
    var pickUpDate: CLong?
    var dropOffDate: CLong?
    var pickUpLocation: String?
    var dropOffLocation: String?
    var accepted: Bool?
    var hasReview: Bool?
    
    init(withDictionary dictionary: NSDictionary) {
        pdf = dictionary["pdf"] as? String
        id = dictionary["id"] as? CLong
        pickUpDate = dictionary["pickUpDate"] as? CLong
        dropOffDate = dictionary["dropOffDate"] as? CLong
        pickUpLocation = dictionary["pickUpLocation"] as? String
        dropOffLocation = dictionary["dropOffLocation"] as? String
        accepted = dictionary["accepted"] as? Bool
        hasReview = dictionary["hasReview"] as? Bool
    }
}
