//
//  LocationResponse.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 19/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class LocationResponse: NSObject {
    var success: Bool?
    var message: String?
    var data: [Location] = []
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        
        for location in dictionary["data"] as! [NSDictionary] {
            data.append(Location(withDictionary: location))
        }
    }
}

class Location: NSObject {
    var id: Int?
    var name: String?
    
    init(withDictionary dictionary: NSDictionary) {
        id = dictionary["id"] as? Int
        name = dictionary["name"] as? String
    }
}
