//
//  ReservationsByIdsRequest.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 10/06/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit

class ReservationsByIdsRequest: Codable {
    var ids: [CLong] = [CLong]()
}
