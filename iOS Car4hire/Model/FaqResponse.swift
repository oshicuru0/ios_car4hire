//
//  FaqResponse.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 22/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class FaqResponse: NSObject {
    var success: Bool?
    var message: String?
    var data: [Faq] = []
    
    init(withDictionary dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        
        for faq in dictionary["data"] as! [NSDictionary] {
            data.append(Faq(withDictionary: faq))
        }
    }
}

class Faq {
    var question: String?
    var answer: String?
    
    init(withDictionary dictionary: NSDictionary) {
        question = dictionary["question"] as? String
        answer = dictionary["answer"] as? String
    }
}
