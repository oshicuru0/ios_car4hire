//
//  SearchRequest.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 19/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class SearchRequest: NSObject {
    var pickUpLocationId: String?
    var dropOffLocationId: String?
    var pickUpTime: String?
    var dropOffTime: String?
    var currency: String = "BAM"
}
