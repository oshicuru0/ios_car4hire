//
//  App.swift
//  iOS_car4hire
//
//  Created by Osman Rešidović on 10.03.20.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class App {

    static let shared = App()
    
    var accessToken: String?
    var appVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    var languageCode: String {
        var code = Locale.current.languageCode ?? "en"
        if let shortCode = code.split(separator: "-").first {
            code = String(shortCode)
        }
        if !Constants.supportedLanguages.contains(code) {
            code = "en"
        }
        return code
    }
}
