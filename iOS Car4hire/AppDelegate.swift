//
//  AppDelegate.swift
//  iOS_car4hire
//
//  Created by Osman Rešidović on 10.03.20.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Bundle.setLanguage(UserDefaults.standard.string(forKey: Constants.APP_LANGUAGES) ?? "en")
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

extension UIViewController {
    static var alert: UIAlertController = UIAlertController(title: nil, message: NSLocalizedString("please_wait", comment: ""), preferredStyle: .alert)
    
    func showProgress() {
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        UIViewController.alert.view.addSubview(loadingIndicator)
        self.present(UIViewController.alert, animated: false)
    }
    
    func dismissProgress() {
        UIViewController.alert.dismiss(animated: false)
    }
}
