//
//  ContactViewController.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 04/08/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(named: "primary")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        title = NSLocalizedString("contact", comment: "")
        navigationController?.title = NSLocalizedString("contact", comment: "")
    }
    
    @IBAction func tapOnCallUs(_ sender: Any) {
        guard let number = URL(string: "tel://0038761381115") else { return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func tapOnEmail(_ sender: Any) {
        guard let url = URL(string: "mailto:support@car4hire.ba") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func tapOnWebsite(_ sender: Any) {
        guard let url = URL(string: "https://car4hire.ba") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func tapOnViber(_ sender: Any) {
        guard let url = URL(string: "viber://contact?number=38761381115") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func tapOnWhatsApp(_ sender: Any) {
        guard let url = URL(string: "https://wa.me/38761381115") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
