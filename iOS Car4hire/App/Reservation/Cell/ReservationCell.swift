//
//  ReservationCell.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 10/06/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit

class ReservationCell: UITableViewCell {
    
    static let TAG = "reservation_cell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickUpDate: UIButton!
    @IBOutlet weak var pickUpTime: UIButton!
    
    @IBOutlet weak var leaveReview: UIButton!
    @IBOutlet weak var dropOffDate: UIButton!
    @IBOutlet weak var dropOffTime: UIButton!
    
    private let dateFormatter = DateFormatter()
    var completion: ((_ reservationId: CLong?) -> Void)!
    
    var reservationWithPdf: ReservationWithPdf? {
        didSet {
            if let reservation = reservationWithPdf, let resId = reservation.id {
                titleLabel.text = "#\(resId)"
                
                pickUpDate.setTitle(reservationWithPdf?.pickUpLocation ?? "", for: UIControl.State.normal)
                dropOffDate.setTitle(reservationWithPdf?.dropOffLocation ?? "", for: UIControl.State.normal)
                
                dateFormatter.dateFormat = "dd MMM YYYY k:mm"
                pickUpTime.setTitle(
                    dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval.init(exactly: (reservation.pickUpDate ?? 1000) / 1000)!))
                    , for: UIControl.State.normal)
                
                dropOffTime.setTitle(
                    dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval.init(exactly: (reservation.dropOffDate ?? 1000) / 1000)!))
                    , for: UIControl.State.normal)
            }
            leaveReview.setTitle(NSLocalizedString("leave_review", comment: ""), for: UIControl.State.normal)
            leaveReview.isHidden = reservationWithPdf?.hasReview ?? true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func tap_on_leave_review(_ sender: Any) {
        self.completion(reservationWithPdf?.id)
    }
}
