//
//  PDFViewerViewController.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 12/06/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit
import WebKit

class PDFViewerViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    var pdfLink: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_navigation_back"), style: .plain, target: self, action:#selector(onBack))
        backButton.tintColor = UIColor(named: "black")
        navigationItem.leftBarButtonItem = backButton
        
        let url: URL! = URL(string: pdfLink ?? "")
        webView.load(URLRequest(url: url))
        
        title = NSLocalizedString("reservation", comment: "")
        navigationController?.title = NSLocalizedString("reservations", comment: "")
    }
    
    @objc func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         navigationController?.popViewController(animated: false)
    }
}
