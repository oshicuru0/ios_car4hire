//
//  ReservationViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 04/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class ReservationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var placeholder: UILabel!
    var tableData: [ReservationWithPdf] = [ReservationWithPdf]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("reservations", comment: "")
        navigationController?.title = NSLocalizedString("reservations", comment: "")
        
        navigationController?.navigationBar.barTintColor = UIColor(named: "primary")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
        
        placeholder.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showProgress()
        
        ReservationService.shared.getReservation { (response, error) in
            DispatchQueue.main.async {
                self.dismissProgress()
                self.tableData = response?.data.reversed() ?? [ReservationWithPdf]()
                self.tableView.reloadData()
                if response?.data.isEmpty ?? true {
                    self.placeholder.isHidden = false
                } else {
                    self.placeholder.isHidden = true
                }
            }
        }
    }
}

extension ReservationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReservationCell.TAG, for: indexPath) as! ReservationCell
        cell.reservationWithPdf = tableData[indexPath.row]
        cell.completion = {(reservationId) in
            let vc = UIStoryboard(name: "Popup", bundle: Bundle.main).instantiateViewController(withIdentifier: "LeaveReviewViewController") as? LeaveReviewViewController
            vc?.modalPresentationStyle = .overFullScreen
            vc?.reservationId = reservationId
            vc?.completition = {() in
                self.viewWillAppear(false)
            }
            self.present(vc!, animated: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableData[indexPath.row].accepted == false {
            DialogHelper.showPopUp(controller: self, title: NSLocalizedString("reservation_not_accepted_title", comment: ""), message: NSLocalizedString("reservation_not_accepted_message", comment: ""))
            return
        }
        
        let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDFViewerViewController") as? PDFViewerViewController
        controller?.pdfLink = tableData[indexPath.row].pdf
        navigationController?.pushViewController(controller!, animated: false)
    }
}
