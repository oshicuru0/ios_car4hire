//
//  LeaveReviewViewController.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 11/08/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit
import AARatingBar

class LeaveReviewViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var reviewField: UITextField!
    
    @IBOutlet weak var friendlinessLabel: UILabel!
    @IBOutlet weak var friendliness: AARatingBar!
    @IBOutlet weak var frienlinessValue: UILabel!
    
    @IBOutlet weak var cleanlinessLabel: UILabel!
    @IBOutlet weak var cleanliness: AARatingBar!
    @IBOutlet weak var cleanlinessValue: UILabel!
    
    @IBOutlet weak var dropoffLabel: UILabel!
    @IBOutlet weak var dropoff: AARatingBar!
    @IBOutlet weak var dropoffValue: UILabel!
    
    @IBOutlet weak var sendButton: UIButton!
    
    var reservationId: CLong?
    var completition: (()->Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = NSLocalizedString("leave_feedback_to_company", comment: "")
        name.text = NSLocalizedString("name_or_alias", comment: "")
        review.text = NSLocalizedString("review", comment: "")
        friendlinessLabel.text = NSLocalizedString("friendliness", comment: "")
        cleanlinessLabel.text = NSLocalizedString("vehicle_cleanliness", comment: "")
        dropoffLabel.text = NSLocalizedString("dropoff_time", comment: "")
        sendButton.setTitle(NSLocalizedString("send_review", comment: ""), for: UIControl.State.normal)
        reviewField.placeholder = NSLocalizedString("enter_your_review", comment: "")
        nameField.placeholder = NSLocalizedString("enter_your_name_or_alias", comment: "")
        
        friendliness.value = 3
        cleanliness.value = 3
        dropoff.value = 3
        
        friendliness.ratingDidChange = {(value) in
            self.frienlinessValue.text = value.description
        }
        
        cleanliness.ratingDidChange = {(value) in
            self.cleanlinessValue.text = value.description
        }
        
        dropoff.ratingDidChange = {(value) in
            self.dropoffValue.text = value.description
        }
        
        reviewField.delegate = self
        nameField.delegate = self
    }
    
    @IBAction func tap_on_close(_ sender: Any) {
        dismiss(animated: false)
    }
    
    @IBAction func tap_on_send(_ sender: Any) {
        if nameField.text?.isEmpty ?? true {
            DialogHelper.showPopUp(controller: self,
                                   title: NSLocalizedString("error", comment: ""),
                                   message: NSLocalizedString("name_is_required", comment: ""))
            return
        }
        
        if reviewField.text?.isEmpty ?? true {
            DialogHelper.showPopUp(controller: self,
                                   title: NSLocalizedString("error", comment: ""),
                                   message: NSLocalizedString("review_is_required", comment: ""))
            return
        }
        
        let feedback = Feedback()
        feedback.id=nil
        feedback.name = self.nameField.text
        feedback.review = self.reviewField.text
        feedback.reservationId = self.reservationId
        feedback.date = CLong(Date().timeIntervalSince1970 * 1000)
        let rating = Rating()
        rating.friendliness = Double(self.friendliness.value)
        rating.vehicleCleanliness = Double(self.cleanliness.value)
        rating.dropOffTime = Double(self.dropoff.value)
        rating.overallValue = 0
        rating.id = nil
        feedback.rate = rating
        showProgress()
        FeedbackService.shared.leaveReview(feedback: feedback) { (response, error) in
            DispatchQueue.main.async {
                self.dismissProgress()
                DialogHelper.showPopUp(controller: self, title: "Thank you!", message: "Review is submitted!") {(alert) in
                    self.dismiss(animated: false)
                    self.completition()
                }
            }
        }
    }
}

extension LeaveReviewViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
