//
//  FilterPopUpViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 22/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol FilterUpdateDelegate: class {
    func onUpdate(filterData: FilterData)
}

protocol CheckBoxDelegate: class{
    func onChangeState(section: Int, value: String, state: Bool)
}

class FilterPopUpViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterByLabel: UILabel!
    @IBOutlet weak var updateResultsButton: UIButton!
    
    var tableData: [Int: FilterSection] = [Int : FilterSection]()
    
    var filterDelegate: FilterUpdateDelegate?
    
    var filterData: FilterData = FilterData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let section1 = FilterSection()
        section1.header = NSLocalizedString("car_category", comment: "")
        section1.items = [
            NSLocalizedString("Economy", comment: ""),
            NSLocalizedString("Compact", comment: ""),
            NSLocalizedString("Standard/Intermediate", comment: ""),
            NSLocalizedString("Van/Minivan", comment: ""),
            NSLocalizedString("SUV/Crossover", comment: ""),
            NSLocalizedString("Luxury/Premium", comment: ""),
            NSLocalizedString("Convertible", comment: "")
        ]
        
        let section2 = FilterSection()
        section2.header = NSLocalizedString("supplier", comment: "")
        section2.items = []
        for p in AppData.shared.products ?? [] {
            if let name = p.supplier?.name, !(section2.items?.contains(name) ?? false) {
                section2.items?.append(name)
            }
        }
        
        let section3 = FilterSection()
        section3.header = NSLocalizedString("car_specification", comment: "")
        section3.items = [
            NSLocalizedString("petrol", comment: ""),
            NSLocalizedString("disel", comment: ""),
            NSLocalizedString("gas", comment: ""),
            NSLocalizedString("automatic", comment: ""),
            NSLocalizedString("manual", comment: "")
        ]
        
        tableData[0] = section1
        tableData[1] = section2
        tableData[2] = section3
        
        tableView.delegate = self
        tableView.dataSource = self
        
        updateResultsButton.setTitle(NSLocalizedString("update_results", comment: ""), for: UIControl.State.normal)
        filterByLabel.text = NSLocalizedString("filter_by", comment: "")
    }
    
    @IBAction func tap_on_Close(_ sender: Any) {
        dismiss(animated: false)
    }
    
    @IBAction func tap_on_Update(_ sender: Any) {
        dismiss(animated: false)
        filterDelegate?.onUpdate(filterData: filterData)
    }
}

extension FilterPopUpViewController: CheckBoxDelegate {
    
    func onChangeState(section: Int, value: String, state: Bool) {
        switch section {
        case 0: if state { filterData.carTypes.append(value) } else { filterData.carTypes.removeAll { (val) -> Bool in val == value } }
        case 1: if state { filterData.suppliers.append(value) } else { filterData.suppliers.removeAll { (val) -> Bool in val == value } }
        case 2: if state { filterData.carSpecs.append(value) } else { filterData.carSpecs.removeAll { (val) -> Bool in val == value } }
        default:
            break
        }
    }
}

extension FilterPopUpViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkbox_cell_id", for: indexPath) as! CheckBoxCell
        cell.checkbox.setTitle(tableData[indexPath.section]?.items?[indexPath.row] ?? "", for: UIControl.State.normal)
        cell.checkbox.tag = indexPath.section
        switch indexPath.section {
        case 0:
            cell.identificator = ["Economy", "Compact", "Standard/Intermediate", "Van/Minivan", "SUV/Crossover", "Luxury/Premium", "Convertible"][indexPath.row]
            cell.checkbox.isSelected = self.filterData.carTypes.contains(cell.identificator ?? "")
        case 1:
            cell.identificator = tableData[indexPath.section]?.items?[indexPath.row] ?? ""
            cell.checkbox.isSelected = filterData.suppliers.contains(cell.identificator ?? "")
        case 2:
            cell.identificator = ["Petrol", "Disel", "Gas", "Automatic", "Manual"][indexPath.row]
            cell.checkbox.isSelected = filterData.carSpecs.contains(cell.identificator ?? "")
        default: break
        }
        cell.checkBoxDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section]?.items?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let button = UIButton()
        button.setTitle(tableData[section]?.header ?? "", for: UIControl.State.normal)
        button.setTitleColor(UIColor(named: "accent"), for: UIControl.State.normal)
        button.contentHorizontalAlignment = .left;
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16);
        return button
    }
}

class CheckBoxCell: UITableViewCell {
    
    static let TAG = "CheckBoxCell"
    
    @IBOutlet weak var checkbox: UIButton!
    
    var checkBoxDelegate: CheckBoxDelegate?
    var identificator: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func tap_on_CheckBox(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        checkBoxDelegate?.onChangeState(section: checkbox.tag, value: identificator ?? "", state: sender.isSelected)
    }
}

class FilterSection {
    var header: String?
    var items: [String]?
}

class FilterData {
    var suppliers: [String] = []
    var carTypes: [String] = []
    var carSpecs: [String] = []
}
