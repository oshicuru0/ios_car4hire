//
//  DatePickerViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 04/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol DatePickerDelegate: class {
    func onOk(picker: DatePickerViewController, date: Date)
}

class DatePickerViewController: UIViewController {
    
    weak var delegate: DatePickerDelegate?
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    var minDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loc = Locale(identifier: UserDefaults.standard.string(forKey: Constants.APP_LANGUAGES) ?? "en")
        self.datePicker.locale = loc
        datePicker.minimumDate = minDate
        cancelButton.setTitle(NSLocalizedString("cancel", comment: ""), for: UIControl.State.normal)
        okButton.setTitle(NSLocalizedString("ok", comment: ""), for: UIControl.State.normal)
    }
    
    @IBAction func tap_on_Ok(_ sender: Any) {
        dismiss(animated: false)
        delegate?.onOk(picker: self, date: datePicker.date)
    }
    @IBAction func tap_on_Cancel(_ sender: Any) {
        dismiss(animated: false)
    }
}
