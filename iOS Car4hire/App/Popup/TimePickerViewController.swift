//
//  TimePickerViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 19/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol TimePickerDelegate: class {
    func onOk(picker: TimePickerViewController, date: Date)
}

class TimePickerViewController: UIViewController {

     weak var delegate: TimePickerDelegate?
        
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loc = Locale(identifier: UserDefaults.standard.string(forKey: Constants.APP_LANGUAGES) ?? "en")
        self.timePicker.locale = loc
        cancelButton.setTitle(NSLocalizedString("cancel", comment: ""), for: UIControl.State.normal)
        okButton.setTitle(NSLocalizedString("ok", comment: ""), for: UIControl.State.normal)
    }
    
    @IBAction func tap_on_Ok(_ sender: Any) {
        delegate?.onOk(picker: self, date: timePicker.date)
        dismiss(animated: false)
    }
    @IBAction func tap_on_Cancel(_ sender: Any) {
        dismiss(animated: false)
    }
}
