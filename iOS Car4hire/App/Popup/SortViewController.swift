//
//  SortViewController.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 10/08/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit

class SortViewController: UIViewController {
    
    @IBOutlet weak var sortByLabel: UILabel!
    
    @IBOutlet weak var recommended: UIButton!
    @IBOutlet weak var high: UIButton!
    @IBOutlet weak var low: UIButton!
    var completion: ((_ type: Int) -> Void)!
    var currentType = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch currentType {
        case 0:
            recommended.isSelected = true
            break
        case 2:
            high.isSelected = true
             break
        default:
            low.isSelected = true
        }
        recommended.setTitle(NSLocalizedString("sort_recommended", comment: ""), for: UIControl.State.normal)
        high.setTitle(NSLocalizedString("sort_high_to_low", comment: ""), for: UIControl.State.normal)
        low.setTitle(NSLocalizedString("sort_low_to_high", comment: ""), for: UIControl.State.normal)
        sortByLabel.text = NSLocalizedString("sort_by", comment: "")
    }
    
    @IBAction func tap_on_recommended(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        high.isSelected = false
        low.isSelected = false
        completion(0)
        dismiss(animated: false)
    }
    
    @IBAction func tap_on_high(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        low.isSelected = false
        recommended.isSelected = false
        completion(2)
        dismiss(animated: false)
    }
    
    @IBAction func tap_on_low(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        recommended.isSelected = false
        high.isSelected = false
        completion(1)
        dismiss(animated: false)
    }
    @IBAction func tap_on_outside(_ sender: Any) {
         dismiss(animated: false)
    }
}
