//
//  WheelViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 23/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol WheelPopupDelegate: class {
    func onOk(tag: String?, count: Int)
}

class WheelViewController: UIViewController{
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var wheelTitle: UILabel!
    
    var dataSource: [String]?
    var wheelTitleLabel: String?
    var TAG: String?
    
    var delegate: WheelPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        wheelTitle.text = wheelTitleLabel ?? "Select"
    }
    
    @IBAction func tap_on_Ok(_ sender: Any) {
        dismiss(animated: false)
        delegate?.onOk(tag: TAG, count: pickerView.selectedRow(inComponent: 0))
    }
}

extension WheelViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        dataSource?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        dataSource?[row] ?? ""
    }
}
