//
//  AppData.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 21/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class AppData {
    static var shared = AppData()
    lazy var products: [Product]? = nil
    lazy var locations: [Location]? = nil
    lazy var searchRequest: SearchRequest? = nil
    lazy var faq: [Faq]? = nil
    lazy var acriss: AcrissData? = nil
    lazy var priceData: PriceData? = nil
    var reservationRequest = ReservationRequest()
    
    func validateAcriss(acriss: String, filterObject: FilterData) -> Bool {
        if acriss.isEmpty {
            return false
        }
        
        let value = Array(acriss) [1]
        if (filterObject.carTypes.isEmpty) {
            return true
        }
        
        if (filterObject.carTypes.contains("Economy")) {
            if (acriss.starts(with:"E") || acriss.starts(with:"H")) {
                return true
            }
        }
        
        if (filterObject.carTypes.contains("Compact")) {
            if (acriss.starts(with:"C") || acriss.starts(with:"D")) {
                return true
            }
        }
        
        if (filterObject.carTypes.contains("Standard/Intermediate")) {
            if (acriss.starts(with:"I") || acriss.starts(with:"J") || acriss.starts(with:"S") || acriss.starts(with: "R")) {
                return true
            }
        }
        
        if (filterObject.carTypes.contains("Van/Minivan")) {
            if (value == "V" || value == "K") {
                return true
            }
        }
        
        if (filterObject.carTypes.contains("SUV/Crossover")) {
            if (value == "F" || value == "J" || value == "P" || value == "Q" || value == "G") {
                return true
            }
        }
        
        if (filterObject.carTypes.contains("Convertible")) {
            if (value == "T") {
                return true
            }
        }
        
        if (filterObject.carTypes.contains("Luxury/Premium")) {
            if (acriss.starts(with: "P") || acriss.starts(with: "U") || acriss.starts(with: "L") || acriss.starts(with: "W")
                ) {
                return true
            }
        }
        
        return false
    }
    
    func doFiltering(filterData: FilterData, sortType: Int) -> [Product] {
        var filteredList: [Product] = []
        
        AppData.shared.products?.forEach({ (product) in
            if let supplierName = product.supplier?.name, let fuelType = product.car?.fuelType, let transmission = product.car?.transmission, let accriss = product.car?.acriss {
                let supplierValidation = (filterData.suppliers.isEmpty) || filterData.suppliers.contains(supplierName)
                
                let otherValidation = (filterData.carSpecs.isEmpty) || filterData.carSpecs.contains(fuelType) || filterData.carSpecs.contains(transmission)
                
                if (supplierValidation && otherValidation && validateAcriss(acriss: accriss, filterObject: filterData))  {
                    filteredList.append(product)
                }
            }
        })
        
        
        return filteredList.sorted { (p1, p2) -> Bool in
            return (sortType == 1 && p1.price?.daily ?? 0 < p2.price?.daily ?? 0) || (sortType == 2 && p1.price?.daily ?? 0 > p2.price?.daily ?? 0)
        }
    }
    
    func createAcriss(acriss: String) -> String {
        var value = ""
        for c in AppData.shared.acriss?.category ?? [] {
            if (acriss.starts(with: c.shortName ?? "")) {
                value = c.name ?? ""
                break
            }
        }
        
        for c in AppData.shared.acriss?.type ?? [] {
            if (Array(acriss) [1] == Array(c.shortName!)[0]) {
                value += " - "
                value += c.name ?? ""
                break
            }
        }
        
        return value.isEmpty ? "N/A" : value
    }
}
