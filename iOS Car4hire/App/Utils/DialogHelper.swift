//
//  DialogHelper.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 22/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class DialogHelper {
    static func showPopUp(controller: UIViewController, title: String, message: String, _ completion: ((UIAlertAction)-> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: completion))
        controller.present(alert, animated: true, completion: nil)
    }
}
