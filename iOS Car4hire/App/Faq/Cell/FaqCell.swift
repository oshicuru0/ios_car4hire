//
//  FaqCellTableViewCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 22/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol UpdateFaqCellHeight: class {
    func doUpdate()
}

class FaqCell: UITableViewCell {

   
    @IBOutlet weak var question: UIButton!
    @IBOutlet weak var answer: UILabel!
    var cellUpdateHandler: UpdateFaqCellHeight?
    private var shouldShowAnswer: Bool = false
    
    var faq: Faq? {
        didSet {
            self.question?.setTitle(faq?.question, for: UIControl.State.normal)
            if shouldShowAnswer {
                answer.text = faq?.answer
                question.setImage(UIImage(named: "ic_arrow_up_accent_small"), for: UIControl.State.normal)
            } else {
                answer.text = ""
                question.setImage(UIImage(named: "ic_arrow_down_accent_small"), for: UIControl.State.normal)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func tap_on_Question(_ sender: Any) {
        shouldShowAnswer = !shouldShowAnswer
        cellUpdateHandler?.doUpdate()
    }
}
