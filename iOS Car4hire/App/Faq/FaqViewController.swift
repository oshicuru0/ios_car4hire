//
//  FaqViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 04/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class FaqViewController: UIViewController {
    
    let cellId = "cellId"
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("faq_long", comment: "")
        navigationController?.title = NSLocalizedString("faq_short", comment: "")
        
        navigationController?.navigationBar.barTintColor = UIColor(named: "primary")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
       
        self.showProgress()
        FaqService.shared.fetchFaq { (response, error) in
            AppData.shared.faq = response?.data
            self.tableView.reloadData()
            self.dismissProgress()
        }
    }
}

extension FaqViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! FaqCell
        cell.faq = AppData.shared.faq?[indexPath.row]
        cell.cellUpdateHandler = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppData.shared.faq?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

extension FaqViewController: UpdateFaqCellHeight {
    func doUpdate() {
        tableView.reloadData()
    }
}
