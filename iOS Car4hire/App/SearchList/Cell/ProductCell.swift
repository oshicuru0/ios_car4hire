//
//  ProductTableCellTableViewCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 21/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit
import AARatingBar
import SDWebImage

class ProductCell: UITableViewCell {
    
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var carAcris: UILabel!
    @IBOutlet weak var pricePerDay: UILabel!
    @IBOutlet weak var rating: AARatingBar!
    @IBOutlet weak var supplierLogo: UIImageView!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var deposit: UILabel!
    @IBOutlet weak var ratingAmount: UILabel!
    
    var product: Product? {
        didSet {
            if let product = product, let car = product.car, let supplier = product.supplier, let price = product.price {
                self.carName.text = "\(car.manufacturer ?? "") \(car.model ?? "")"
                self.carImage.sd_setImage(with: URL(string: car.image ?? ""))
                self.pricePerDay.text = "\(product.currency ?? "EUR") \(price.daily?.description ?? "0") / \(NSLocalizedString("day", comment: ""))"
                self.supplierLogo.sd_setImage(with: URL(string: supplier.logo ?? ""))
                self.rating.value = CGFloat(product.rating ?? 0.0)
                self.rating.isEnabled = false
                self.ratingAmount.text = product.rating?.description ?? "0.0"
                self.carAcris.text = AppData.shared.createAcriss(acriss: car.acriss ?? "")
                self.deposit.text = "\(NSLocalizedString("deposit", comment: "")): \(product.currency ?? "EUR") \(product.depositAmount?.description ?? "")"
                
                switch product.depositType {
                case "card":
                    self.deposit.text = "\(NSLocalizedString("deposit", comment: "")): \(NSLocalizedString("deposit_type_card", comment: ""))"
                    break
                    
                case "cash":
                    self.deposit.text = "\(NSLocalizedString("deposit", comment: "")): \(product.currency ?? "EUR") \(product.depositAmount?.description ?? "")"
                    break
                default:
                    self.deposit.text = "\(NSLocalizedString("deposit", comment: "")): \(NSLocalizedString("deposit_type_no", comment: ""))"
                    
                }
            }
        }
    }
}
