//
//  FeedbackCellTableViewCell.swift
//  iOS Car4hire
//
//  Created by Osman Residovic on 09/08/2020.
//  Copyright © 2020 Car4Hire. All rights reserved.
//

import UIKit
import AARatingBar

class FeedbackCell: UITableViewCell {
    static let TAG = "feedback_cell"
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var friendliness: AARatingBar!
    @IBOutlet weak var friendlinessValue: UILabel!
    @IBOutlet weak var cleanliness: AARatingBar!
    @IBOutlet weak var cleanlinessValue: UILabel!
    @IBOutlet weak var dropOffTime: AARatingBar!
    @IBOutlet weak var dropOffValue: UILabel!
    @IBOutlet weak var overall: AARatingBar!
    @IBOutlet weak var overallValue: UILabel!
    
    private let dateFormatter = DateFormatter()
    var feedback: Feedback? {
        didSet {
            dateFormatter.dateFormat = "dd.MMM.yyyy"
            if let feedback = feedback {
                name.text = feedback.name
                date.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval((feedback.date ?? 1) / 1000)))
                message.text = feedback.review
                
                friendliness.value = CGFloat(feedback.rate?.friendliness ?? 0)
                friendlinessValue.text = feedback.rate?.friendliness?.description
                
                cleanliness.value = CGFloat(feedback.rate?.vehicleCleanliness ?? 0)
                cleanlinessValue.text = feedback.rate?.vehicleCleanliness?.description
                
                dropOffTime.value = CGFloat(feedback.rate?.dropOffTime ?? 0)
                dropOffValue.text = feedback.rate?.dropOffTime?.description
                
                overall.value = CGFloat(feedback.rate?.overallValue ?? 0)
                overallValue.text = feedback.rate?.overallValue?.description
            }
        }
    }
}
