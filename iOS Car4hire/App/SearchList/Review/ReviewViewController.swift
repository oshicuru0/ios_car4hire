//
//  ReviewPopupViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var list: [Feedback] = []
    var supplierId: CLong?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("feedback", comment: "")
        
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_navigation_back"), style: .plain, target: self, action:#selector(onBack))
        navigationItem.leftBarButtonItem = backButton
        
        showProgress()
        FeedbackService.shared.fetchFeedback(supplierId: supplierId?.description ?? "") { (response, error) in
            self.dismissProgress()
            if let list = response?.data {
                self.list = list
                self.tableView.reloadData()
            }
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @objc func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tap_on_Close(_ sender: Any) {
        dismiss(animated: false)
    }
}

extension ReviewViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedbackCell.TAG, for: indexPath) as! FeedbackCell
        cell.feedback = list[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}
