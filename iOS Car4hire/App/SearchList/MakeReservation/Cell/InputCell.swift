//
//  InputCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 26/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class InputCell: UITableViewCell {
    static let TAG = "input_cell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    var tableView: UITableView?
    var value: String? {
        didSet {
            textField.text = value
        }
    }
    
    var title: String?{
        didSet{
            if let title = title {
                titleLabel.text = NSLocalizedString(title, comment: "")
                textField.placeholder = "\(NSLocalizedString("enter_your", comment: "")) \(NSLocalizedString(title, comment: "").lowercased())"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification , object:nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification , object:nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardHeight = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func getPair() -> KeyValue {
        return KeyValue(key: title, value: textField.text)
    }
}

class KeyValue  {
    var key: String?
    var value: String?
    
    init(key: String?, value: String?) {
        self.key = key
        self.value = value
    }
}

extension InputCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
