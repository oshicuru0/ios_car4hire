//
//  ButtonCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 26/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol ButtonDelegate {
    func onClick()
}
class PaymentButtonCell: UITableViewCell{
    static let TAG = "payment_button_cell"
    
    @IBOutlet weak var button: UIButton!
    
    var controller: UIViewController?
    var delegate: ButtonDelegate?
    var title: String? {
        didSet{
            if let title = title {
                button.setTitle(title, for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func tap_on_Payment(_ sender: Any) {
        delegate?.onClick()
    }
}
