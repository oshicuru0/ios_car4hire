//
//  MakeReservationViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 26/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class MakeReservationViewController: UIViewController {
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var listCells: [UITableViewCell] = [UITableViewCell]()
    var product: Product?
    var priceData: PriceData?
    var userData: [String] = ["First name","Last name", "Email", "Country of residence", "Mobile phone", "City of residence", "Birth date", "Flight number"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("make_reservation", comment: "")
        
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_navigation_back"), style: .plain, target: self, action:#selector(onBack))
        navigationItem.leftBarButtonItem = backButton
        
        setCells()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @objc func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func setCells() {
        if let product = product, let carImage = product.car?.image {
            
            self.carImage.sd_setImage(with: URL(string: carImage))
            
            listCells.append(tableView.dequeueReusableCell(withIdentifier: CarImageCell.TAG) as! CarImageCell)
            
            let controlCell = tableView.dequeueReusableCell(withIdentifier: ControllCell.TAG) as! ControllCell
            controlCell.product = product
            controlCell.controller = self
            listCells.append(controlCell)
            
            let priceCell = tableView.dequeueReusableCell(withIdentifier: PriceCell.TAG) as! PriceCell
            priceCell.priceData = priceData
            listCells.append(priceCell)
            
            for u in userData {
                let inputCell = tableView.dequeueReusableCell(withIdentifier: InputCell.TAG) as! InputCell
                inputCell.title = u
                inputCell.tableView = tableView
                inputCell.value = UserDefaults.standard.string(forKey: u)
                listCells.append(inputCell)
            }
            
            let paymentButton = tableView.dequeueReusableCell(withIdentifier: PaymentButtonCell.TAG) as! PaymentButtonCell
            paymentButton.delegate = self
            paymentButton.title = NSLocalizedString("proceed_to_payment", comment: "")
            listCells.append(paymentButton)
        }
    }
}

extension MakeReservationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return listCells[indexPath.row]
    }
}

extension MakeReservationViewController: ButtonDelegate {
    
    func onClick() {
        for c in listCells{
            if c is InputCell {
                if let inputCell = c as? InputCell{
                    if inputCell.getPair().value?.isEmpty == true && "Flight number" != inputCell.getPair().key{
                        DialogHelper.showPopUp(controller: self, title: NSLocalizedString("error", comment: ""), message: "\(NSLocalizedString(inputCell.getPair().key ?? "", comment: "")) \(NSLocalizedString("is_required", comment: ""))")
                       return
                    } else {
                        switch inputCell.getPair().key {
                        case "First name":
                            AppData.shared.reservationRequest.firstName = inputCell.getPair().value
                        case "Last name":
                            AppData.shared.reservationRequest.lastName = inputCell.getPair().value
                        case "Email":
                            AppData.shared.reservationRequest.email = inputCell.getPair().value
                        case "Country of residence":
                            AppData.shared.reservationRequest.country = inputCell.getPair().value
                        case "Mobile phone":
                            AppData.shared.reservationRequest.phoneNumber = inputCell.getPair().value
                        case "City of residence":
                            AppData.shared.reservationRequest.cityOfResidence = inputCell.getPair().value
                        case "Birth date":
                            AppData.shared.reservationRequest.dateOfBirth = inputCell.getPair().value
                        default:
                            AppData.shared.reservationRequest.flightNumber = inputCell.getPair().value
                        }
                    }
                }
            }
        }
        
        
        UserDefaults.standard.set(AppData.shared.reservationRequest.firstName, forKey: Constants.UserDataKeys.FIRST_NAME_KEY)
        UserDefaults.standard.set(AppData.shared.reservationRequest.lastName, forKey: Constants.UserDataKeys.LAST_NAME_KEY)
        UserDefaults.standard.set(AppData.shared.reservationRequest.email, forKey: Constants.UserDataKeys.EMAIL_KEY)
        UserDefaults.standard.set(AppData.shared.reservationRequest.country, forKey: Constants.UserDataKeys.COUNTRY_KEY)
        UserDefaults.standard.set(AppData.shared.reservationRequest.cityOfResidence, forKey: Constants.UserDataKeys.CITY_KEY)
        UserDefaults.standard.set(AppData.shared.reservationRequest.dateOfBirth, forKey: Constants.UserDataKeys.BIRTH_KEY)
        UserDefaults.standard.set(AppData.shared.reservationRequest.phoneNumber, forKey: Constants.UserDataKeys.MOBILE_KEY)
        
        /* let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
         vc.modalPresentationStyle = .fullScreen
         vc.priceData = priceData
         self.navigationController?.pushViewController(vc, animated: true)*/
        
        self.showProgress()
        ReservationService.shared.postReservation { (response, error) in
            var ids: String = UserDefaults.standard.string(forKey: Constants.UserDataKeys.RESERVATIONS) ?? ""
            ids.append(";")
            ids.append(response?.resrvation.id?.description ?? "0")
            UserDefaults.standard.set(ids, forKey: Constants.UserDataKeys.RESERVATIONS)
            DispatchQueue.main.async {
                self.dismissProgress()
                let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FinalUIViewController") as! FinalUIViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.reservationIdText = NSLocalizedString("reservation_id_title", comment: "") + ": #\(response?.resrvation.id?.description ?? "")"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
