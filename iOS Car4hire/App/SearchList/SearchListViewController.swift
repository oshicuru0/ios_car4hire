//
//  SearchListViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 02/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit
import DropDown

class SearchListViewController: UIViewController {
    
    let cellId = "cellId"
    
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var dropOffLocationLabel: UILabel!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var dropOffDateLabel: UILabel!
    @IBOutlet weak var pickUpTimeLabel: UILabel!
    @IBOutlet weak var dropOffTimeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickUpDetailsLabel: UILabel!
    @IBOutlet weak var dropOffDetailsLabel: UILabel!
    
    private let dateFormatter = DateFormatter()
    private var filterData = FilterData()
    private let sortDropDown = DropDown()
    private var sortType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(named: "primary")
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        title = "Car4Hire"
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(image: UIImage(named: "ic_filter"), style: .plain, target: self, action: #selector(onFilter)),UIBarButtonItem(image: UIImage(named: "ic_sort"), style: .plain, target: self, action: #selector(onSort))]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_navigation_back"), style: .plain, target: self, action:#selector(onBack))
        
        tableView.dataSource = self
        tableView.delegate = self
        
        if let loc = AppData.shared.locations?.first(where: {$0.id?.description == AppData.shared.searchRequest?.pickUpLocationId}) {
            self.pickUpLocationLabel.text = loc.name
        }
        
        if let loc = AppData.shared.locations?.first(where: {$0.id?.description == AppData.shared.searchRequest?.dropOffLocationId}) {
            self.dropOffLocationLabel.text = loc.name
        }
        
        dateFormatter.dateFormat = "dd.MMM"
        pickUpDateLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval.init(exactly: CLong(AppData.shared.searchRequest!.pickUpTime ?? "0")!/1000)!))
        dropOffDateLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval.init(exactly: CLong(AppData.shared.searchRequest!.dropOffTime ?? "0")!/1000)!))
        dateFormatter.dateFormat = "k:mm"
        pickUpTimeLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval.init(exactly: CLong(AppData.shared.searchRequest!.pickUpTime ?? "0")!/1000)!))
        dropOffTimeLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval.init(exactly: CLong(AppData.shared.searchRequest!.dropOffTime ?? "0")!/1000)!))
        
        pickUpDetailsLabel.text = NSLocalizedString("pick_up_details", comment: "")
        dropOffDetailsLabel.text = NSLocalizedString("drop_off_details", comment: "")
    }
    
    func doSort(index: Int) {
        sortType = index
        tableView.reloadData()
    }
    
    @objc func onBack() {
        dismiss(animated: true)
    }
    
    @objc func onFilter() {
        let vc = UIStoryboard(name: "Popup", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterPopUpViewController") as! FilterPopUpViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.filterData = self.filterData
        vc.filterDelegate = self
        present(vc, animated: false)
    }
    
    @objc func onSort() {
        let vc = UIStoryboard(name: "Popup", bundle: Bundle.main).instantiateViewController(withIdentifier: "SortViewController") as! SortViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.currentType = self.sortType
        vc.completion = {(type) -> Void in
            self.doSort(index: type)
        }
        present(vc, animated: false)
    }
}

extension SearchListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ProductCell
        cell.product = AppData.shared.doFiltering(filterData: filterData, sortType: sortType)[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppData.shared.doFiltering(filterData: filterData, sortType: sortType).count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectProductViewController") as! SelectProductViewController
        vc.modalPresentationStyle = .fullScreen
        vc.product = (tableView.cellForRow(at: indexPath) as! ProductCell).product
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SearchListViewController: FilterUpdateDelegate {
    
    func onUpdate(filterData: FilterData) {
        self.filterData = filterData
        tableView.reloadData()
    }
}
