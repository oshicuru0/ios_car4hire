//
//  SelectProductViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 23/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class SelectProductViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var carImageView: UIImageView!
    
    var product: Product?
    var listCells: [UITableViewCell] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Car4Hire"
        
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_navigation_back"), style: .plain, target: self, action:#selector(onBack))
        navigationItem.leftBarButtonItem = backButton
        
        
        if let car = product?.car {
            carImageView.sd_setImage(with: URL(string: car.image ?? ""))
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        AppData.shared.reservationRequest.productId = product?.id
        createCells()
    }
    
    @objc func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func createCells() {
        listCells.append(tableView.dequeueReusableCell(withIdentifier: CarImageCell.TAG) as! CarImageCell)
        
        let controlCell = tableView.dequeueReusableCell(withIdentifier: ControllCell.TAG) as! ControllCell
        controlCell.product = product
        controlCell.controller = self
        listCells.append(controlCell)
        
        let priceCell = tableView.dequeueReusableCell(withIdentifier: PriceCell.TAG) as! PriceCell
        let priceData = PriceData()
        priceData.payNowPrice = product?.price?.payNow
        priceData.extrasPrice = 0.00
        priceData.atRentalPrice = product?.price?.toSupplier
        priceData.rentalPrice = (product?.price?.daily ?? 1.0) * Double(product!.price!.days!)
        priceData.currency = product?.currency
        priceData.totalPrice = product!.price!.daily! *  Double(product!.price!.days!)
        priceData.totalPriceSmall = product?.price?.daily
        priceData.days = product?.price?.days
        priceCell.priceData = priceData
        AppData.shared.priceData = priceData
        listCells.append(priceCell)
        
        let extrasCell = tableView.dequeueReusableCell(withIdentifier: ExtrasCell.TAG) as! ExtrasCell
        extrasCell.extras = product?.extras
        extrasCell.controller = self
        extrasCell.chanageExtraDelegate = self
        listCells.append(extrasCell)
        
        let carSpecCell = tableView.dequeueReusableCell(withIdentifier: CarSpecCell.TAG) as! CarSpecCell
        carSpecCell.car = product?.car
        listCells.append(carSpecCell)
        
        
        if let extras = product?.supplier?.extras {
            if !extras.isEmpty {
                let titleCell = tableView.dequeueReusableCell(withIdentifier: TitleCell.TAG) as! TitleCell
                listCells.append(titleCell)
                var index = 0
                while index < extras.count {
                    let supplierExtrasCell = tableView.dequeueReusableCell(withIdentifier: SupplierExtrasCell.TAG) as! SupplierExtrasCell
                    supplierExtrasCell.labels = [extras[index].name ?? nil]
                    
                    if index + 1 < extras.count {
                        supplierExtrasCell.labels?.append(extras[index + 1].name ?? "")
                    }
                    listCells.append(supplierExtrasCell)
                    index += 2
                }
            }
        }
        
        let buttonCell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.TAG) as! ButtonCell
        buttonCell.delegate = self
        listCells.append(buttonCell)
    }
}

extension SelectProductViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return listCells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCells.count
    }
}

extension SelectProductViewController: ChangeExtraDelegate {
    
    func onChnaged(type: String?, amount: Double) {
        if let priceCell = listCells[2] as? PriceCell {
            let priceData = priceCell.priceData
            if let priceData = priceData {
                switch type {
                case "gps":
                    priceData.gpsValue = amount
                case "childSeat":
                    priceData.childSeatValue = amount
                default:
                    priceData.driverValue = amount
                }
                
                priceData.extrasPrice = priceData.childSeatValue + priceData.driverValue + priceData.gpsValue
                priceData.totalPrice = product!.price!.daily! *  Double(product!.price!.days!) + priceData.extrasPrice!
                priceData.totalPriceSmall = (product?.price?.daily)! + priceData.extrasPrice!
                priceData.atRentalPrice = (product?.price?.toSupplier ?? 0.0) + priceData.extrasPrice!
                
                priceCell.priceData = priceData
                AppData.shared.priceData = priceData
            }
        }
    }
}

extension SelectProductViewController: NextButtonDelegate {
    
    func onClicked() {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MakeReservationViewController") as! MakeReservationViewController
        vc.modalPresentationStyle = .fullScreen
        vc.product = product
        vc.priceData = AppData.shared.priceData
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
