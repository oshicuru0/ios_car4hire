//
//  CarSpecCellTableViewCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class CarSpecCell: UITableViewCell {
 static let TAG = "car_spec_cell"
    
    @IBOutlet weak var luggage: UIButton!
    @IBOutlet weak var person: UIButton!
    @IBOutlet weak var doors: UIButton!
    
    @IBOutlet weak var air: UIButton!
    @IBOutlet weak var transmission: UIButton!
    @IBOutlet weak var fuel: UIButton!
    
    var car: Car? {
        didSet {
            if let car = car {
                luggage.setTitle(car.luggage!, for: UIControl.State.normal)
                person.setTitle(car.seats!, for: UIControl.State.normal)
                doors.setTitle(car.doors!, for: UIControl.State.normal)
                air.setTitle(NSLocalizedString("air_title", comment: ""), for: UIControl.State.normal)
                transmission.setTitle(car.transmission!.prefix(1).description == "M" ? NSLocalizedString("manual", comment: "") : NSLocalizedString("automatic", comment: ""), for: UIControl.State.normal)
                fuel.setTitle(NSLocalizedString(car.fuelType?.lowercased() ?? "", comment: ""), for: UIControl.State.normal)
            }
        }
    }
}
