//
//  SupplierExtrasCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class SupplierExtrasCell: UITableViewCell {
    
    static let TAG = "supplier_extras_cell"
    
    @IBOutlet weak var leftLabel: UIButton!
    @IBOutlet weak var rightLabel: UIButton!
    
    var labels: [String?]? {
        didSet {
            let left: String? = labels? [0]
            
            if labels?.count ?? 0 > 1 {
                let right: String? = labels? [1]
                if let right = right {
                    rightLabel.isHidden = false
                    rightLabel.setTitle(NSLocalizedString(right, comment: ""), for: UIControl.State.normal)
                }
            } else {
                 rightLabel.isHidden = true
            }
            
            
            if let left = left {
                leftLabel.setTitle(NSLocalizedString(left, comment: ""), for: UIControl.State.normal)
            }
        }
    }
}
