//
//  ButtonCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol NextButtonDelegate: class {
    func onClicked()
}

class ButtonCell: UITableViewCell{
    
    static var TAG = "button_cell"
    var delegate: NextButtonDelegate?
    
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        button.setTitle(NSLocalizedString("next", comment: ""), for: UIControl.State.normal)
    }
    
    @IBAction func tap_on_Next(_ sender: Any) {
        delegate?.onClicked()
    }
}
