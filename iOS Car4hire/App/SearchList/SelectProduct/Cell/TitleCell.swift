//
//  TitleCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {
 static let TAG = "title_cell"
 @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = NSLocalizedString("free_extras", comment: "")
    }
}
