//
//  PriceCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {
    static let TAG = "price_cell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var rentalPrice: UILabel!
    @IBOutlet weak var extrasPrice: UILabel!
    @IBOutlet weak var totalPriceSmall: UILabel!
    @IBOutlet weak var payNowPrice: UILabel!
    @IBOutlet weak var atRentalPrice: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var firstValue: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var secondValue: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var thirdValue: UILabel!
    
    @IBOutlet weak var rentalCostLabel: UILabel!
    @IBOutlet weak var extrasLabel: UILabel!
    @IBOutlet weak var childSeatLabel: UILabel!
    @IBOutlet weak var additionalDriverLabel: UILabel!
    @IBOutlet weak var gpsLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var payNowLabel: UILabel!
    @IBOutlet weak var atRentalLabel: UILabel!
    
    var priceData: PriceData? {
        didSet {
            if let priceData = priceData {
                totalPrice.text = "\(priceData.currency!) \(priceData.totalPrice?.description ?? "")"
                rentalPrice.text = "\(priceData.currency!) \(priceData.rentalPrice?.description ?? "")"
                extrasPrice.text = "\(priceData.currency!) \(priceData.extrasPrice?.description ?? "")"
                totalPriceSmall.text = "\(priceData.currency!) \(priceData.totalPriceSmall?.description ?? "")"
               /* payNowPrice.text = "\(priceData.currency!) \(priceData.payNowPrice?.description ?? "")"
                atRentalPrice.text = "\(priceData.currency!) \(priceData.atRentalPrice?.description ?? "")"*/
                var daysLabel =  ""
                if priceData.days! > 1 {daysLabel = NSLocalizedString("days", comment: "")} else {daysLabel = NSLocalizedString("day", comment: "")}
                titleLabel.text = "\(NSLocalizedString("total_rental_price_for", comment: "")) \(priceData.days?.description ?? "") \(daysLabel)"
                
                firstValue.text = "\(priceData.currency!) \(priceData.childSeatValue.description)"
                secondValue.text = "\(priceData.currency!) \(priceData.gpsValue.description)"
                thirdValue.text = "\(priceData.currency!) \(priceData.driverValue.description)"
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rentalCostLabel.text = NSLocalizedString("rental_cost", comment: "")
        extrasLabel.text = NSLocalizedString("extras", comment: "")
        childSeatLabel.text = NSLocalizedString("child_seat", comment: "")
        additionalDriverLabel.text = NSLocalizedString("additional_driver", comment: "")
        gpsLabel.text = NSLocalizedString("gps", comment: "")
        totalLabel.text = NSLocalizedString("total", comment: "")
        /*payNowLabel.text = NSLocalizedString("pay_now", comment: "")
        atRentalLabel.text = NSLocalizedString("at_rental_company", comment: "")*/
    }
}


class PriceData {
    var totalPrice: Double?
    var rentalPrice: Double?
    var extrasPrice: Double?
    var totalPriceSmall: Double?
    var payNowPrice: Double?
    var atRentalPrice: Double?
    var currency: String?
    var days: Int?
    var gpsValue: Double = 0.0
    var driverValue: Double = 0.0
    var childSeatValue: Double = 0.0
}
