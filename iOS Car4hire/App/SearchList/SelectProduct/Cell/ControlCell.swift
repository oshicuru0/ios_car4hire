//
//  ControlCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit
import AARatingBar

class ControllCell: UITableViewCell {
    
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var acris: UILabel!
    @IBOutlet weak var supplierLogo: UIImageView!
    @IBOutlet weak var rating: AARatingBar!
    @IBOutlet weak var ratingValue: UILabel!
    @IBOutlet weak var orSimilarLabel: UILabel!
    @IBOutlet weak var acrisLabel: UILabel!
    @IBOutlet weak var showMoreButton: UIButton!
    
    var controller: UIViewController?
    
    static let TAG = "control_cell"
    var product: Product? {
        didSet {
            if let car = product?.car, let supplier = product?.supplier {
                carName.text = "\(car.manufacturer ?? "") \(car.model ?? "")"
                supplierLogo.sd_setImage(with: URL(string: supplier.logo ?? ""))
                rating.isEnabled = false
                rating.value = CGFloat(product?.rating ?? 0)
                ratingValue.text = product?.rating?.description ?? "0"
                acris.text = AppData.shared.createAcriss(acriss: car.acriss ?? "")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        orSimilarLabel.text = NSLocalizedString("or_similar", comment: "")
        showMoreButton.setTitle(NSLocalizedString("show_more", comment: ""), for: UIControl.State.normal)
    }
    
    @IBAction func tap_on_ShowMore(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController
        vc?.supplierId = product?.supplier?.id
        controller?.navigationController?.pushViewController(vc!, animated: false)
    }
}
