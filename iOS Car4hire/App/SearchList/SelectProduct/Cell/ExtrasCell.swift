//
//  ExtrasCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit
import DropDown

protocol ChangeExtraDelegate: class {
    func onChnaged(type: String?, amount: Double)
}

class ExtrasCell: UITableViewCell {
    
    @IBOutlet weak var childPrice: UILabel!
    @IBOutlet weak var driverPrice: UILabel!
    @IBOutlet weak var gpsPrice: UILabel!
    @IBOutlet weak var childSeatButton: UIButton!
    @IBOutlet weak var driverButton: UIButton!
    @IBOutlet weak var gpsButton: UIButton!
    
    @IBOutlet weak var extrasLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var childSeatLabel: UILabel!
    @IBOutlet weak var gpsLabel: UILabel!
    @IBOutlet weak var additionalDriverLabel: UILabel!
    
    static let TAG = "extras_cell"
    var controller: UIViewController?
    var gpsList: [String] = []
    var childList: [String] = []
    var driverList: [String] = []
    
    var gpsPriceValue: Double?
    var driverPriceValue: Double?
    var childSeatPriceValue: Double?
    var currency: String = "BAM"
    var extras: [Extra]? {
        didSet {
            for e in extras ?? [] {
                if "gps" == e.name {
                    for i in 0...(e.qty ?? 0) {
                        gpsList.append(i.description)
                    }
                    gpsPriceValue = e.price
                }
                if "childSeat" == e.name {
                    for i in 0...(e.qty ?? 0) {
                        childList.append(i.description)
                    }
                    childSeatPriceValue = e.price
                }
                if "additionalDriver" == e.name {
                    for i in 0...(e.qty ?? 0) {
                        driverList.append(i.description)
                    }
                    driverPriceValue = e.price
                }
            }
        }
    }
    
    var chanageExtraDelegate: ChangeExtraDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        extrasLabel.text = NSLocalizedString("extras", comment: "").uppercased()
        qtyLabel.text = NSLocalizedString("qty", comment: "").uppercased()
        priceLabel.text = NSLocalizedString("price", comment: "").uppercased()
        
        childSeatLabel.text = NSLocalizedString("child_seat", comment: "").uppercased()
        gpsLabel.text = NSLocalizedString("gps", comment: "").uppercased()
        additionalDriverLabel.text = NSLocalizedString("additional_driver", comment: "").uppercased()
    }
    
    @IBAction func tap_on_Gps(_ sender: UIButton) {
        let dropDown: DropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = .white
        dropDown.direction = .any
        dropDown.dataSource = gpsList
        dropDown.selectionAction = {(index: Int, item: String) in
            sender.setTitle(item, for: UIControl.State.init())
            self.gpsPrice.text = "\(self.currency) \(self.gpsPriceValue! * Double(index))"
            self.chanageExtraDelegate?.onChnaged(type: "gps", amount:(Double(index) * self.gpsPriceValue!))
            AppData.shared.reservationRequest.gps = index
        }
        dropDown.show()
    }
    
    @IBAction func tap_on_ChildSeat(_ sender: UIButton) {
        let dropDown: DropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = .white
        dropDown.direction = .any
        dropDown.dataSource = childList
        dropDown.selectionAction = {(index: Int, item: String) in
            sender.setTitle(item, for: UIControl.State.init())
            self.childPrice.text = "\(self.currency) \(self.childSeatPriceValue! * Double(index))"
            self.chanageExtraDelegate?.onChnaged(type: "childSeat", amount: (Double(index) * self.childSeatPriceValue!))
            AppData.shared.reservationRequest.childSeat = index
        }
        dropDown.show()
    }
    
    @IBAction func tap_on_Driver(_ sender: UIButton) {
        let dropDown: DropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = .white
        dropDown.direction = .any
        dropDown.dataSource = driverList
        dropDown.selectionAction = {(index: Int, item: String) in
            sender.setTitle(item, for: UIControl.State.init())
            self.driverPrice.text = "\(self.currency) \(self.driverPriceValue! * Double(index))"
            self.chanageExtraDelegate?.onChnaged(type: "driver", amount: (Double(index) * self.driverPriceValue!))
            AppData.shared.reservationRequest.additionalDriver = index
        }
        dropDown.show()
    }
}
