//
//  PayNowCell.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 26/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol PayButtonDelegate: class {
    func onPayClicked()
}

class PayNowCell: UITableViewCell {
    static let TAG = "pay_now_cell"
    
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var payNowButton: UIButton!
    
    var delegate: PayButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        noteLabel.text = NSLocalizedString("payment_note", comment: "")
        payNowButton.setTitle(NSLocalizedString("pay_now", comment: ""), for: UIControl.State.normal)
    }
    
    @IBAction func tap_on_Pay(_ sender: Any) {
        delegate?.onPayClicked()
    }
}
