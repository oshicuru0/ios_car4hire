//
//  PaymentViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 26/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    var priceData: PriceData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("payment_review", comment: "")
        
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_navigation_back"), style: .plain, target: self, action:#selector(onBack))
        backButton.tintColor = UIColor(named: "black")
        navigationItem.leftBarButtonItem = backButton
        
        tableView.delegate = self
        tableView.dataSource = self
        
        progressView.isHidden = true
        progressIndicator.stopAnimating()
    }
    
    @objc func onBack() {
        navigationController?.popViewController(animated: true)
    }
}

extension PaymentViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: PriceCell.TAG, for: indexPath) as! PriceCell
            cell.priceData = priceData
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: PayNowCell.TAG, for: indexPath) as! PayNowCell
            cell.delegate = self
            return cell
        }
    }
}

extension PaymentViewController: PayButtonDelegate{
    
    func onPayClicked() {
        self.progressView.isHidden = false
        self.progressIndicator.startAnimating()
        BraintreeService.shared.fetchBraintreeToken { (response, error) in
            
            if let clientId = response?.clientId {
                BraintreeHelper.shared.braintreeDropIn(clientTokenOrTokenizationKey: clientId, controller: self) { (success, nonce) in
                    AppData.shared.reservationRequest.nonce = nonce
                    ReservationService.shared.postReservation { (response, error) in
                        var ids: String = UserDefaults.standard.string(forKey: Constants.UserDataKeys.RESERVATIONS) ?? ""
                        ids.append(";")
                        ids.append(response?.resrvation.id?.description ?? "0")
                        UserDefaults.standard.set(ids, forKey: Constants.UserDataKeys.RESERVATIONS)
                        self.progressView.isHidden = true
                        self.progressIndicator.stopAnimating()
                        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FinalUIViewController") as! FinalUIViewController
                        vc.modalPresentationStyle = .overFullScreen
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
}
