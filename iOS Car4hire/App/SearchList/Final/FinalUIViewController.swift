//
//  FinalUIViewController.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 27/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class FinalUIViewController: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var reservationId: UILabel!
    var reservationIdText: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        title = NSLocalizedString("reservation", comment: "")
        reservationId.text = reservationIdText
        okButton.setTitle(NSLocalizedString("go_home", comment: ""), for: UIControl.State.normal)
    }
    
    @IBAction func tap_on_Ok(_ sender: Any) {
        navigationController?.dismiss(animated: true)
    }
}
