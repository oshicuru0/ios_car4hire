//
//  ViewController.swift
//  iOS_car4hire
//
//  Created by Osman Rešidović on 10.03.20.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit
import DropDown

class SearchViewController: UIViewController {
    @IBOutlet weak var currency: UIButton!
    @IBOutlet weak var pickUpSearchBar: UISearchBar!
    @IBOutlet weak var dropOffSearchBar: UISearchBar!
    @IBOutlet weak var differentDropOff: UIButton!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var fromDayLabel: UILabel!
    @IBOutlet weak var toDayLabel: UILabel!
    @IBOutlet weak var fromTime: UIButton!
    @IBOutlet weak var toTime: UIButton!
    @IBOutlet weak var dropOffHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var selectDateTimeLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var toPlaceholderLabel: UILabel!
    @IBOutlet weak var fromPlaceholderLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
    private weak var fromPicker: DatePickerViewController?
    private weak var toPicker: DatePickerViewController?
    private weak var fromTimePicker: TimePickerViewController?
    private weak var toTimePicker: TimePickerViewController?
    
    private let pickUpDropDown = DropDown()
    private let dropOffDropDown = DropDown()
    private var pickUpDate: CLong = Int((Date().timeIntervalSince1970 * 1000) + (1000 * 60 * 60 * 24))
    private var dropOffDate: CLong = Int((Date().timeIntervalSince1970 * 1000) + (1000 * 60 * 60 * 48))
    private var pickUpTime: Date = Date()
    private var dropOffTime: Date = Date()
    private var data: [String] = []
    private var dataFiltered: [String] = []
    private var searchViewModel: SearchViewModel?
    private let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showProgress()
        fillLocalization()
        searchViewModel = SearchViewModel(locationService: LocationService.shared, delegate: self)
        searchViewModel?.fetchData()
        
        
        dropOffHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
        fillTimeFrom(date: Date())
        fillFrom(date: Date(timeIntervalSince1970: TimeInterval.init(exactly: pickUpDate/1000)!))
        fillTo(date: Date(timeIntervalSince1970: TimeInterval.init(exactly: dropOffDate/1000)!))
        fillTimeTo(date: Date())
        
        pickUpDropDown.anchorView = pickUpSearchBar
        pickUpDropDown.bottomOffset = CGPoint(x: 0, y:(pickUpDropDown.anchorView?.plainView.bounds.height)!)
        pickUpDropDown.backgroundColor = .white
        pickUpDropDown.direction = .bottom
        pickUpDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.pickUpSearchBar.text = item
        }
        
        dropOffDropDown.anchorView = dropOffSearchBar
        dropOffDropDown.bottomOffset = CGPoint(x: 0, y:(dropOffDropDown.anchorView?.plainView.bounds.height)! + 50)
        dropOffDropDown.backgroundColor = .white
        dropOffDropDown.direction = .bottom
        dropOffDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.dropOffSearchBar.text = item
        }
        
        pickUpSearchBar.delegate = self
        dropOffSearchBar.delegate = self
    }
    
    func fillLocalization() {
        title = "Car4Hire"
        navigationController?.title = NSLocalizedString("search_title", comment: "")
        navigationController?.navigationBar.barTintColor = UIColor(named: "primary")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "arrow_down_white")?.withRenderingMode(.alwaysTemplate), title: UserDefaults.standard.string(forKey: Constants.UserDataKeys.CURRENCY_KEY) ?? "EUR", target: self, action:#selector(currencyClicked))
        navigationItem.rightBarButtonItem = leftButton
        
        if let language = UserDefaults.standard.string(forKey: Constants.APP_LANGUAGES) {
            let rightButton = UIBarButtonItem(image: UIImage(named: "arrow_down_white")?.withRenderingMode(.alwaysTemplate), title: language, target: self, action:#selector(tap_on_Change))
            navigationItem.leftBarButtonItem = rightButton
        } else {
            let rightButton = UIBarButtonItem(image: UIImage(named: "arrow_down_white")?.withRenderingMode(.alwaysTemplate), title: "en", target: self, action:#selector(tap_on_Change))
            navigationItem.leftBarButtonItem = rightButton
        }
        self.searchButton.setTitle(NSLocalizedString("search", comment: ""), for: UIControl.State.normal)
        self.differentDropOff.setTitle(NSLocalizedString("different_drop_off", comment: ""), for: UIControl.State.normal)
        self.selectDateTimeLabel.text = NSLocalizedString("select_date_time", comment: "")
        self.toPlaceholderLabel.text = NSLocalizedString("to", comment: "")
        self.fromPlaceholderLabel.text = NSLocalizedString("from", comment: "")
        self.pickUpSearchBar.placeholder = NSLocalizedString("enter_pick_up_place", comment: "")
        self.dropOffSearchBar.placeholder = NSLocalizedString("enter_drop_off_place", comment: "")
    }
    
    func fillFrom(date: Date)  {
        dateFormatter.dateFormat = "dd"
        let d = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "MMMM"
        fromLabel.text = "\(d) \(NSLocalizedString(dateFormatter.string(from: date).lowercased(), comment: ""))"
        
        dateFormatter.dateFormat = "EEEE"
        fromDayLabel.text = NSLocalizedString(dateFormatter.string(from: date).lowercased(), comment: "")
        
        pickUpDate = Int(date.timeIntervalSince1970 * 1000)
        
        fillTimeFrom(date: date)
    }
    
    func fillTimeFrom(date: Date)  {
        dateFormatter.dateFormat = "k:mm"
        fromTime.setTitle(dateFormatter.string(from: date), for: UIControl.State.init())
        self.pickUpTime = date
    }
    
    func fillTo(date: Date)  {
        dateFormatter.dateFormat = "dd"
        let d = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "MMMM"
        toLabel.text = "\(d) \(NSLocalizedString(dateFormatter.string(from: date).lowercased(), comment: ""))"
        
        dateFormatter.dateFormat = "EEEE"
        toDayLabel.text = NSLocalizedString(dateFormatter.string(from: date).lowercased(), comment: "")
        
        dropOffDate = Int(date.timeIntervalSince1970 * 1000)
        
        fillTimeTo(date: date)
    }
    
    func fillTimeTo(date: Date)  {
        dateFormatter.dateFormat = "k:mm"
        toTime.setTitle( dateFormatter.string(from: date), for: UIControl.State.init())
        self.dropOffTime = date
    }
    
    @IBAction func search_onTap(_ sender: Any) {
        if !validateFields() {
            return
        }
        
        self.showProgress()
        let searchRequest = SearchRequest()
        var pLocation = ""
        if let loc = AppData.shared.locations?.first(where: {$0.name == pickUpSearchBar.searchTextField.text}) {
            searchRequest.pickUpLocationId = loc.id?.description ?? "0"
            pLocation = loc.name ?? ""
        }
        
        if let loc = AppData.shared.locations?.first(where: {$0.name == dropOffSearchBar.searchTextField.text}) {
            searchRequest.dropOffLocationId = loc.id?.description ?? "0"
        } else {
            searchRequest.dropOffLocationId = searchRequest.pickUpLocationId
        }
        
        searchRequest.currency = UserDefaults.standard.string(forKey: Constants.UserDataKeys.CURRENCY_KEY) ?? "EUR"
        
        
        let pDate1 = Date(timeIntervalSince1970: TimeInterval.init(exactly: self.pickUpDate/1000) ?? 100)
        let dDate1 = Date(timeIntervalSince1970: TimeInterval.init(exactly: self.dropOffDate/1000) ?? 100)
        let cal = Calendar.current
        let p = cal.date(bySettingHour: cal.component(.hour, from: self.pickUpTime), minute: cal.component(.minute, from: self.pickUpTime), second: 0, of: pDate1)!
        let d = cal.date(bySettingHour: cal.component(.hour, from: self.dropOffTime), minute: cal.component(.minute, from: self.dropOffTime), second: 0, of: dDate1)!
        searchRequest.dropOffTime = Int(d.timeIntervalSince1970 * 1000).description
        searchRequest.pickUpTime = Int(p.timeIntervalSince1970 * 1000).description
        
        AppData.shared.searchRequest = searchRequest
        
        UserDefaults.standard.set(pLocation, forKey: Constants.UserDataKeys.PICK_UP_LOCATION_KEY)
        
        ProductService.shared.fetchProducts(s: searchRequest) { (productResponse, error) in
            self.dismissProgress()
            
            if error != nil {
                DispatchQueue.main.async {
                    DialogHelper.showPopUp(controller: self, title: "Error", message: NSLocalizedString("drop_off_in_future_validation_message", comment: ""))
                }
                return
            }
            
            AppData.shared.products = productResponse?.data
            AppData.shared.reservationRequest.pickUpDate = Int(p.timeIntervalSince1970 * 1000)
            AppData.shared.reservationRequest.dropOffDate = Int(d.timeIntervalSince1970 * 1000)
            AppData.shared.reservationRequest.pickUpLocation = Int(searchRequest.pickUpLocationId ?? "0")
            AppData.shared.reservationRequest.dropOffLocation = Int(searchRequest.dropOffLocationId ?? "0")
            AppData.shared.reservationRequest.currency = UserDefaults.standard.string(forKey: Constants.UserDataKeys.CURRENCY_KEY) ?? "EUR"
            
            DispatchQueue.main.async {
                let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchListNavigationController")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            }
        }
    }
    
    @objc func currencyClicked() {
        let dropDown: DropDown = DropDown()
        dropDown.anchorView = navigationItem.rightBarButtonItem
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = .white
        dropDown.direction = .bottom
        dropDown.dataSource = ["EUR", "USD", "BAM"]
        dropDown.selectionAction = {(index: Int, item: String) in
            UserDefaults.standard.set(item, forKey: Constants.UserDataKeys.CURRENCY_KEY)
            (self.navigationItem.rightBarButtonItem?.customView as? UIButton)?.setTitle(item, for: UIControl.State.normal)
        }
        dropDown.show()
    }
    
    @IBAction func dropOff_onTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            dropOffHeightConstraint.constant = 56
            self.view.layoutIfNeeded()
        } else {
            dropOffHeightConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func from_onTap(_ sender: Any) {
        fromPicker = UIStoryboard(name: "Popup", bundle: Bundle.main).instantiateViewController(withIdentifier: "DatePickerViewController") as? DatePickerViewController
        fromPicker?.modalPresentationStyle = .overFullScreen
        fromPicker?.delegate = self
        fromPicker?.minDate = Date(timeIntervalSince1970: TimeInterval.init(exactly: pickUpDate/1000) ?? 100)
        present(fromPicker!, animated: false)
    }
    
    @IBAction func to_onTap(_ sender: Any) {
        toPicker = UIStoryboard(name: "Popup", bundle: Bundle.main).instantiateViewController(withIdentifier: "DatePickerViewController") as? DatePickerViewController
        toPicker?.modalPresentationStyle = .overFullScreen
        toPicker?.delegate = self
        toPicker?.minDate = Date(timeIntervalSince1970: TimeInterval.init(exactly: pickUpDate/1000 + 24*60*60) ?? 100)
        present(toPicker!, animated: false)
    }
    
    @IBAction func fromTime_onTap(_ sender: Any) {
        fromTimePicker = UIStoryboard(name: "Popup", bundle: Bundle.main).instantiateViewController(withIdentifier: "TimePickerViewController") as? TimePickerViewController
        fromTimePicker?.modalPresentationStyle = .overFullScreen
        fromTimePicker?.delegate = self
        present(fromTimePicker!, animated: false)
    }
    
    @IBAction func toTime_onTap(_ sender: Any) {
        toTimePicker = UIStoryboard(name: "Popup", bundle: Bundle.main).instantiateViewController(withIdentifier: "TimePickerViewController") as? TimePickerViewController
        toTimePicker?.modalPresentationStyle = .overFullScreen
        toTimePicker?.delegate = self
        present(toTimePicker!, animated: false)
    }
    
    func validateFields() -> Bool {
        let pickUpLocation = AppData.shared.locations?.first(where: {$0.name == pickUpSearchBar.searchTextField.text})
        if pickUpLocation == nil {
            DialogHelper.showPopUp(controller: self, title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("pick_up_validation_message", comment: ""))
            return false
        }
        
        if differentDropOff.isSelected {
            let dropOffLocation = AppData.shared.locations?.first(where: {$0.name == dropOffSearchBar.searchTextField.text})
            if dropOffLocation == nil {
                DialogHelper.showPopUp(controller: self, title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("drop_off_validation_message", comment: ""))
                return false
            }
        }
        
        if dropOffDate - pickUpDate < 24 * 60 * 60 * 60 {
            DialogHelper.showPopUp(controller: self, title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("drop_off_in_future_validation_message", comment: ""))
            return false
        }
        return true
    }
    
    
    @objc func tap_on_Change() {
        let dropDown: DropDown = DropDown()
        dropDown.anchorView = navigationItem.leftBarButtonItem
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = .white
        dropDown.direction = .bottom
        dropDown.dataSource = [NSLocalizedString("english", comment: ""), NSLocalizedString("bosnian", comment: "")]
        dropDown.selectionAction = {(index: Int, item: String) in
            self.changeLanguageTo(language: index == 0 ? "en" : "bs")
        }
        dropDown.show()
    }
}

extension SearchViewController: DatePickerDelegate {
    
    func onOk(picker: DatePickerViewController, date: Date) {
        if picker == fromPicker {
            fillFrom(date: date)
        } else if picker == toPicker {
            fillTo(date: date)
        }
    }
}

extension SearchViewController: TimePickerDelegate {
    
    func onOk(picker: TimePickerViewController, date: Date) {
        if picker == fromTimePicker {
            fillTimeFrom(date: date)
        } else if picker == toTimePicker {
            fillTimeTo(date: date)
        }
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? data : data.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        if searchBar == pickUpSearchBar {
            if dataFiltered.isEmpty {
                pickUpDropDown.hide()
            } else {
                pickUpDropDown.dataSource = dataFiltered
                pickUpDropDown.show()
            }
        } else {
            if dataFiltered.isEmpty {
                dropOffDropDown.hide()
            } else {
                dropOffDropDown.dataSource = dataFiltered
                dropOffDropDown.show()
            }
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension SearchViewController: SearchViewModelDelegate{
    func didLoadData(_ locations: [Location]?) {
        self.dismissProgress()
        locations?.forEach{
            self.data.append($0.name ?? "")
        }
        
        AppData.shared.locations = locations
        self.pickUpSearchBar.searchTextField.text = UserDefaults.standard.string(forKey: Constants.UserDataKeys.PICK_UP_LOCATION_KEY) ?? ""
    }
    
    func didFail(code: Int, description: String) {
        DispatchQueue.main.async {
            self.dismissProgress()
            DialogHelper.showPopUp(
                controller: self,
                title: NSLocalizedString("error", comment: ""),
                message: NSLocalizedString(code == -1001 ? "network_error_1" : "network_error" , comment: "")
            )
        }
    }
}

extension SearchViewController {
    func changeLanguageTo(language: String) {
        UserDefaults.standard.set(language, forKey: Constants.APP_LANGUAGES)
        UserDefaults.standard.synchronize()
        Bundle.setLanguage(language)
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = storyboard.instantiateInitialViewController()
    }
}

extension UIBarButtonItem {
    convenience init(image: UIImage?, title: String, target: Any?, action: Selector?) {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.contentHorizontalAlignment = .left
        button.semanticContentAttribute = .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        button.frame = CGRect(x: 0, y: 0, width: image!.size.width + 50, height: image!.size.height)
        
        if let target = target, let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        
        self.init(customView: button)
    }
}
