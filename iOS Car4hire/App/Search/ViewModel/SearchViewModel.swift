//
//  SearchViewModel.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 19/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

protocol SearchViewModelDelegate: class {
    func didLoadData(_ locations: [Location]?)
    func didFail(code: Int, description: String)
}

class SearchViewModel {
    
    private var delegate: SearchViewModelDelegate?
    private var locationService: LocationService!
    
    init(locationService: LocationService, delegate: SearchViewModelDelegate) {
        self.delegate = delegate
        self.locationService = locationService
    }
    
    func fetchData() {
        AcrissService.shared.fetchAcriss()
        
        self.locationService.fetchLocations {[weak self] (response, error) in
            guard let strongSelf = self else { return }
            if let e = error {
                DispatchQueue.main.async {
                    strongSelf.delegate?.didFail(code: e.code, description: e.localizedDescription)
                }
                return
            }
           
            strongSelf.delegate?.didLoadData(response?.data)
        }
    }
}
