//
//  BaseService.swift
//  iOS_car4hire
//
//  Created by Osman Rešidović on 10.03.20.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class BaseService: NSObject {
    
    private let timeoutInterval: TimeInterval = 60.0
    
    private var languageCode: String {
        return UserDefaults.standard.string(forKey: Constants.APP_LANGUAGES) ?? "en"
    }
    
    public func getJsonFromUrl(username: String? = nil, password: String? = nil, urlString: String, completion: @escaping (AnyObject?, NSError?)->()) {
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(nil, NSError())
            }
            return
        }
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
        request.httpMethod = "GET"
        
        /*
        if let username = username, let password = password {
            let loginString = String(format: "%@:%@", username, password)
            let loginData = loginString.data(using: String.Encoding.utf8)!
            let base64LoginString = loginData.base64EncodedString()
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        } else {
            if let token = App.shared.accessToken {
                request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }
        request.setValue(languageCode, forHTTPHeaderField: "language")
        request.setValue(App.shared.appVersion, forHTTPHeaderField: "App-Version")
        */
        request.setValue("8ec42fa9-2ef8-4f05-ac62-ddec984a5ab9", forHTTPHeaderField: "AccessToken")
        request.setValue(languageCode, forHTTPHeaderField: "Language")
        print("BaseService -> fetching JSON from URL: \(url)")
        self.executeRequest(request, completion: completion)
    }
    
    public func postDataToUrl(_ postString: String, urlString: String, completion: @escaping (AnyObject?, NSError?)->()) {
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(nil, NSError())
            }
            return
        }
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)
        
        if let token = App.shared.accessToken {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        request.setValue(languageCode, forHTTPHeaderField: "language")
        request.setValue(App.shared.appVersion, forHTTPHeaderField: "App-Version")
        request.setValue("8ec42fa9-2ef8-4f05-ac62-ddec984a5ab9", forHTTPHeaderField: "AccessToken")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON

        print("BaseService -> posting data to URL: \(url)")
        print("BaseService -> request body: \(postString)")
        self.executeRequest(request, completion: completion)
    }
    
    public func postDataAndDownloadFile(_ dictionary: [String:Any], urlString: String, completion: @escaping (AnyObject?, NSError?)->()) {
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(nil, NSError())
            }
            return
        }
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
        request.httpMethod = "POST"
        
        let postString = BaseService.createPostBody(params: dictionary)
        request.httpBody = postString.data(using: .utf8)
        
        if let token = App.shared.accessToken {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        request.setValue(languageCode, forHTTPHeaderField: "language")
        request.setValue(App.shared.appVersion, forHTTPHeaderField: "App-Version")
        print("BaseService -> posting data to URL: \(url)")
        print("BaseService -> request body: \(dictionary)")
        self.executeDownloadRequest(request, completion: completion)
    }
    
    public func postImageToUrl(image: UIImage, filename: String, dictionary: [String:Any], urlString: String, completion: @escaping (AnyObject?, NSError?)->()) {
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(nil, NSError())
            }
            return
        }
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        
        request.httpBody = BaseService.createMultipartBody(parameters: dictionary, boundary: boundary, data: image.jpegData(compressionQuality: 0.7)!, mimeType: "image/jpg", filename: filename)
        
        if let token = App.shared.accessToken {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        request.setValue(languageCode, forHTTPHeaderField: "language")
        request.setValue(App.shared.appVersion, forHTTPHeaderField: "App-Version")
        print("BaseService -> posting data to URL: \(url)")
        print("BaseService -> request body: \(dictionary)")
        self.executeRequest(request, completion: completion)
    }
    
    private static func createMultipartBody(parameters: [String: Any],
                                            boundary: String,
                                            data: Data,
                                            mimeType: String,
                                            filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.append(Data(boundaryPrefix.utf8))
            body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
            body.append(Data("\(value)\r\n".utf8))
        }
        
        body.append(Data(boundaryPrefix.utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(filename)\"; filename=\"\(filename).jpg\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimeType)\r\n\r\n".utf8))
        body.append(data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--".appending(boundary.appending("--")).utf8))
        
        return body as Data
    }
    
    private static func createPostBody(params:[String:Any]) -> String {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    
    private func executeRequest(_ request: URLRequest, completion: @escaping (AnyObject?, NSError?)->()) {
        let urlSessionConfig = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: urlSessionConfig, delegate: nil, delegateQueue: nil)
        urlSession.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            if let error = error {
                print("BaseService -> error: \(error.localizedDescription)")
                completion(nil, NSError(domain: "NetworkError", code: error._code, userInfo: ["message": "network_error", "description": error.localizedDescription]))
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                print("BaseService -> error: \(Constants.Errors.badResponse)")
                completion(nil, Constants.Errors.badResponse)
                return
            }
            if httpResponse.statusCode == 300 {
                print("BaseService -> token has changed")
                guard let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary, let token = jsonObj["token"] as? String else {
                    print("BaseService -> error: \(Constants.Errors.tokenError)")
                    completion(nil, Constants.Errors.tokenError)
                    return
                }
                DispatchQueue.main.async {
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(token, forKey: "token")
                }
                App.shared.accessToken = token
                
                print("BaseService -> token has saved, retrying the request")
                var newRequest = request
                newRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                newRequest.setValue(self.languageCode, forHTTPHeaderField: "language")
                self.executeRequest(newRequest, completion: completion)
                return
            }
            if httpResponse.statusCode >= 400 {
                var errorMessage = "Unknown error ocurred."
                var errorDescription = ""
                

                if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary, let message = jsonObj["message"] as? String, let description = jsonObj["description"] as? String {
                    errorMessage = message
                    errorDescription = description
                }
                print("BaseService -> error: \(errorMessage) \(errorDescription)")
                completion(nil, NSError(domain: "NetworkError", code: httpResponse.statusCode, userInfo: ["message": errorMessage, "description": errorDescription]))
                return
            }
            guard let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as AnyObject else {
                print("BaseService -> error: \(Constants.Errors.serializationError)")
                completion(nil, Constants.Errors.serializationError)
                return
            }
            print("BaseService -> response: \(String(describing: jsonObj))")
            DispatchQueue.main.async {
                completion(jsonObj, nil)
            }
        }).resume()
    }
    
    private func executeDownloadRequest(_ request: URLRequest, completion: @escaping (AnyObject?, NSError?)->()) {
        let urlSessionConfig = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: urlSessionConfig, delegate: nil, delegateQueue: nil)
        urlSession.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let error = error {
                print("BaseService -> error: \(error.localizedDescription)")
                completion(nil, NSError(domain: "NetworkError", code: error._code, userInfo: ["message": "network_error", "description": error.localizedDescription]))
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                print("BaseService -> error: \(Constants.Errors.badResponse)")
                completion(nil, Constants.Errors.badResponse)
                return
            }
            guard let tempLocalUrl = tempLocalUrl, error == nil else {
                print("BaseService -> error: \(Constants.Errors.badResponse)")
                completion(nil, Constants.Errors.noDataError)
                return
            }
            print("BaseService -> Download file finished")
            do {
                let downloadedData = try Data(contentsOf: tempLocalUrl)
                DispatchQueue.main.async(execute: {
                    print("transfer completion OK!")

                    let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first! as NSString
                    let destinationPath = documentDirectoryPath.appendingPathComponent("\(Date().timeIntervalSince1970).pdf")

                    let pdfFileURL = URL(fileURLWithPath: destinationPath)
                    FileManager.default.createFile(atPath: pdfFileURL.path,
                                                   contents: downloadedData,
                                                   attributes: nil)

                    if FileManager.default.fileExists(atPath: pdfFileURL.path) {
                        DispatchQueue.main.async {
                            completion(pdfFileURL as AnyObject, nil)
                        }
                        return
                    }
                    print("BaseService -> error: \(Constants.Errors.badResponse)")
                    completion(nil, Constants.Errors.noDataError)
                    return
                })
            } catch {
                print("BaseService -> error: \(Constants.Errors.badResponse)")
                completion(nil, Constants.Errors.noDataError)
                return
            }
        }.resume()
    }
}

extension BaseService: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        //let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        //completionHandler(.useCredential, urlCredential)
    }
}
