//
//  FaqService.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 22/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class FaqService: BaseService {
    
    class var shared: FaqService {
        return FaqService()
    }
    
    public func fetchFaq(_ completion: @escaping (FaqResponse?, NSError?) -> ()) {
        
        getJsonFromUrl(urlString: "https://car4hire.ba/api/faq/list") {
            json, error in guard let data = json as? NSDictionary, error == nil else {
                completion(nil, error)
                return
            }
            
            completion(FaqResponse(withDictionary: data), nil)
        }
    }
}
