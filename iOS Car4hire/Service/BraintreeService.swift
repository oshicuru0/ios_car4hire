//
//  BraintreeService.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 27/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class BraintreeService: BaseService {
    class var shared: BraintreeService {
        return BraintreeService()
    }
    
    public func fetchBraintreeToken(_ completion: @escaping (BraintreeResponse?, NSError?) -> ()) {
        
        getJsonFromUrl(urlString: "https://car4hire.ba/api/pay/clientToken") {
            json, error in guard let data = json as? NSDictionary, error == nil else {
                completion(nil, error)
                return
            }
            
            completion(BraintreeResponse(withDictionary: data), nil)
        }
    }
}
