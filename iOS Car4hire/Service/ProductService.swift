//
//  ProductService.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 21/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class ProductService: BaseService {
    class var shared: ProductService {
        return ProductService()
    }
    
    public func fetchProducts(s: SearchRequest, _ completion: @escaping (ProductResponse?, NSError?) -> ()) {
        var url = "https://car4hire.ba/api/product/searchProducts"
        url.append("?")
        url.append("pickUpLocation=")
        url.append(s.pickUpLocationId ?? "")
        url.append("&dropOffLocation=")
        url.append(s.dropOffLocationId ?? "")
        url.append("&pickUpDate=")
        url.append(s.pickUpTime ?? "")
        url.append("&dropOffDate=")
        url.append(s.dropOffTime ?? "")
        url.append("&currency=")
        url.append(s.currency)
        
        getJsonFromUrl(urlString: url) {
            json, error in guard let data = json as? NSDictionary, error == nil else {
                completion(nil, error)
                return
            }
            
            completion(ProductResponse(withDictionary: data), nil)
        }
    }
}
