//
//  TestService.swift
//  iOS_car4hire
//
//  Created by Osman Rešidović on 10.03.20.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import Foundation

class LocationService: BaseService {
    
    class var shared: LocationService {
        return LocationService()
    }
    
    public func fetchLocations(_ completion: @escaping (LocationResponse?, NSError?) -> ()) {
        getJsonFromUrl(urlString: "https://car4hire.ba/api/location/list") {
            json, error in guard let data = json as? NSDictionary, error == nil else {
                completion(nil, error)
                return
            }
            
            completion(LocationResponse(withDictionary: data), nil)
        }
    }
}
