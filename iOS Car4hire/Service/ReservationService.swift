//
//  ReservationService.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 27/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class ReservationService: BaseService {
    class var shared: ReservationService {
        return ReservationService()
    }
    
    public func postReservation(_ completion: @escaping (ReservationResponse?, NSError?) -> ()) {
        if let jsonData = try? JSONEncoder().encode(AppData.shared.reservationRequest){
            let json: String = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
            
            postDataToUrl(json, urlString: "https://car4hire.ba/api/reservation/makeReservation") {
                json, error in guard let data = json as? NSDictionary, error == nil else {
                    completion(nil, error)
                    return
                }
                
                completion(ReservationResponse(withDictionary: data), nil)
            }
        }
    }
    
    public func getReservation(_ completion: @escaping (ReservationsByIdsResponse?, NSError?) -> ()) {
        let reservationByIds = ReservationsByIdsRequest()
        let ids = UserDefaults.standard.string(forKey: Constants.UserDataKeys.RESERVATIONS)?.split(separator: ";")
        ids?.forEach({ (substring) in
            reservationByIds.ids.append(CLong(substring.description) ?? 0)
        })
        
        if let jsonData = try? JSONEncoder().encode(reservationByIds){
            let json: String = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
            
            postDataToUrl(json, urlString: "https://car4hire.ba/api/reservation/byIds") {
                json, error in guard let data = json as? NSDictionary, error == nil else {
                    completion(nil, error)
                    return
                }
                
                completion(ReservationsByIdsResponse(withDictionary: data), nil)
            }
        }
    }
}
