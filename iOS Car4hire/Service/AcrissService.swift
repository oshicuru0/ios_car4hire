//
//  AcrissService.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class AcrissService: BaseService {
    
    class var shared: AcrissService {
        return AcrissService()
    }
    
    public func fetchAcriss() {
        
        getJsonFromUrl(urlString: "https://car4hire.ba/api/acriss/list") {
            json, error in guard let data = json as? NSDictionary, error == nil else {
                return
            }
            
            AppData.shared.acriss = AcrissResponse(withDictionary: data).data
        }
    }
}
