//
//  FeedbackService.swift
//  iOS Car4hire
//
//  Created by Osman Rešidović on 25/05/2020.
//  Copyright © 2020 OsmanR. All rights reserved.
//

import UIKit

class FeedbackService: BaseService {
    class var shared: FeedbackService {
        return FeedbackService()
    }
    
    public func fetchFeedback(supplierId: String, _ completion: @escaping (FeedBackResponse?, NSError?) -> ()) {
        
        getJsonFromUrl(urlString: "https://car4hire.ba/api/supplier/review?supplierId=\(supplierId)") {
            json, error in guard let data = json as? NSDictionary, error == nil else {
                completion(nil, error)
                return
            }
            
            completion(FeedBackResponse(withDictionary: data), nil)
        }
    }
    
    public func leaveReview(feedback: Feedback, _ completion: @escaping (FeedBackResponse?, NSError?) -> ()) {
           if let jsonData = try? JSONEncoder().encode(feedback){
               let json: String = String(data: jsonData, encoding: String.Encoding.utf8) ?? ""
               
            postDataToUrl(json, urlString: "https://car4hire.ba/api/supplier/review/\(feedback.reservationId!)/add") {
                   json, error in guard let data = json as? NSDictionary, error == nil else {
                       completion(nil, error)
                       return
                   }
                   
                   completion(FeedBackResponse(withDictionary: data), nil)
               }
           }
       }
}
